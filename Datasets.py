import numpy as np
from array import array
from _struct import unpack


def GetData():
    
    input = np.array([[1,1,1,0,0,0],[1,1,1,0,0,0],[1,1,1,0,0,0],[0,0,0,1,1,1], [0,0,0,1,1,1],[0,0,0,1,1,1]])
    target = np.array([[1], [1], [1], [0], [0], [0]])
    
    return input, target


def GetXORdata():
    
    input = np.array([[0,0], [0,1], [1,0], [1,1]])
    target = np.array([[0], [1], [1], [0]])
    
    return input, target


def GetSPECTdata(type = "train"):
    
    path = "D:\MSc Data Analytics - DT228 A\Dissertation\Facial Recognition\Programming Stuff\Test Binary Data"

    if type == "test":
        # TEST Data set - 187 samples with 23 features. 1st feature is the class label
        data = np.empty(shape=(187, 23))
        path_file = "\SPECT_test.txt"
    else:
        # TRAIN Data set - 80 samples with 23 features. 1st feature is the class label
        data = np.empty(shape=(80, 23))
        path_file = "\SPECT_train.txt"
        
    with open(path + path_file, "rb") as file:
        index = 0
        for line in file:
            data[index, :] = map(int, line.replace("\r\n", "").split(","))
            index += 1
    
    # All but first column as it the class label
    input = data[:,1:]
    # Just the fist column
    target = data[:,0:1]
    
    return input, target
    
   
"""Single label classification problem"""  
def GetKYOTOdata_Raw():
    
    path = "D:\MSc Data Analytics - DT228 A\Dissertation\Facial Recognition\Robert Stuff\WSU3_n100p1.0-Simplified.txt"

    input = np.empty(shape=(12417, 89))
    # 2nd parameter in shape is the number of classes
    target = np.zeros(shape=(12417, 8))
        
    with open(path, "r") as file:
        index = 0
        for line in file:
            # Because first row is a header row
            if not index == 0:
                vector = map(int, line.replace("\n", "").split("\t"))
                # Converting every column to boolean data type except 1st column which is the target
                input[index-1, :] = map(bool, vector[1:])
                # Activating Nth unit for N target value
                target[index-1, vector[0] - 1] = 1
                
            index += 1
    
    return input, target


"""Multi-label classification problem"""
def GetKYOTOdata():
    
    path = "D:\MSc Data Analytics - DT228 A\Dissertation\Facial Recognition\Robert Stuff\WSU3__n=100_po=-1_modification=null_p=-1.0_Simplified.txt"

    input = np.empty(shape=(7577, 89))
    # 2nd parameter in shape is the number of classes
    target = np.empty(shape=(7577, 8))
    # for sampling
    stratify = np.empty(shape=(7577, 1))
        
    with open(path, "r") as file:
        index = 0
        for line in file:
            # Because first row is a header row
            if not index == 0:
                vector = map(int, line.replace("\n", "").split("\t"))
                # column 1-8 = Target Variables
                target[index-1, :] = map(bool, vector[:8])
                # column 9 = Stratification variable
                stratify[index-1] = vector[8]
                # column 10-... = Descriptive features
                input[index-1, :] = map(bool, vector[9:])
                
            index += 1
    
    return input, target


def GetKYOTOdata_Sampled():
    
    path = "D:\MSc Data Analytics - DT228 A\Dissertation\Facial Recognition\Robert Stuff\WSU3__n=100_po=-1_modification=null_p=-1.0_Simplified.txt"
    
    data = np.empty(shape=(7580, 98))
        
    with open(path, "r") as file:
        index = 0
        for line in file:
            # Because first row is a header row
            if not index == 0:
                vector = map(int, line.replace("\n", "").split("\t"))
                # target variables
                data[index-1, :8] = map(bool, vector[:8])
                # stratification variable
                data[index-1, 8] = vector[8]
                # input features
                data[index-1, 9:] = map(bool, vector[9:])
                
            index += 1
    
    np.random.shuffle(data)
    # 90% of data
    train = data[0:6822, :]
    # remaining 10% data
    test = data[6822:7580]
    
    return train[:, 9:], train[:, :8], test[:, 9:], test[:, :8]


def GetSCAREdata_Sampled():
    
    path = "D:\MSc Data Analytics - DT228 A\Dissertation\Facial Recognition\Robert Stuff\SCARE__n=100_po=-1_modification=null_p=-1.0_Simplified.txt"
    
    data = np.empty(shape=(1470, 54))
        
    with open(path, "r") as file:
        index = 0
        for line in file:
            # Because first row is a header row
            if not index == 0:
                vector = map(int, line.replace("\n", "").split("\t"))
                # target variables
                data[index-1, :5] = map(bool, vector[:5])
                # stratification variable
                data[index-1, 5] = vector[5]
                # input features
                data[index-1, 6:] = map(bool, vector[6:])
                
            index += 1
    
    np.random.shuffle(data)
    # 90% of data
    train = data[0:1323, :]
    # remaining 10% data
    test = data[1323:1470]
    
    return train[:, 6:], train[:, :5], test[:, 6:], test[:, :5]
