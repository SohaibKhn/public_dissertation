# Imports
import numpy as np
from RBM import *
from NeuralNet import *
from Datasets import *


class DBN:
    
    """Initializing weights and biases"""
    # weights
    w = []
    # biases
    b = []
    # shape of the network (4,3,5) -> 4 visible units, 3 hidden units, 5 hidden units in last layer
    shape = None
    # Number of layers excluding visible layer
    layerCount = 0
    
    
    """Initialize the network"""
    def __init__(self, shape):
        
        self.layerCount = len(shape) - 1
        self.shape = shape
        
    
    
    """Learn initial values of weights and biases by training RBMs"""
    def PreTrain(self, data):
        
        # For every layer train an RBM
        for layer in range(self.layerCount):
            
            print("Input data for layer {0}: \n {1}\n".format(layer+1, data))
            
            rbm = RBM(number_of_visible_units = self.shape[layer], number_of_hidden_units = self.shape[layer+1])
            rbm.Train(data, epochs = 5000)
            
            self.w.append(rbm.w)
            self.b.append(rbm.b_h)
            
            # Generate the hidden units' states from learned RBM and use them as an input to the next RBM
            data = rbm.Generate(data, "h")
            
            # Remove object to free up the memory
            del(rbm)
            
        print("Weights:\n{0}\n".format(self.w))
        print("Biases:\n{0}\n".format(self.b))
    
    
    
    """Train a deep neural network using pre-trained weights"""
    def Train(self, input, target):
        nn = NeuralNet(self.shape, self.w, self.b)
    
        epochs = 1000
        lnErr = 1e-5
        for epoch in range(epochs+1):
            err = nn.TrainEpoch(input, target)
            
            if epoch % 50 == 0:
                print("Iteration {0}\tError: {1:0.6f}".format(epoch, err))
            if err <= lnErr:
                print("\nMinimum error {0} reached at iteration {1}\n".format(err, epoch))
                break
        return nn
    
    
    
    """"Evaluates"""
    def Evaluate(self, DNN, input, target):
        
        # Run the trained network on Test Data and get the output
        output = DNN.Run(input)
        print("Target:\n{0}\n\n Output:\n{1}".format(target, output))
    
    
        """TopN Accuracy"""
        # Sort the output in ascending order and return the index of elements
        # [20,30,10,50] => [3, 1, 0, 2]
        # Here the indexes represent the output units. In this case highest value 50 is given by the 4th output unit, and least 10 by the 3rd
        outputIndex = np.argsort(output, axis=1)
    
        # Compare the target with the top N output probabilities
        topN_error = 3
        correct = outputIndex[:, -topN_error:] == np.argmax(target, axis=1)[np.newaxis].T
        accuracy = ((np.sum(correct)*1.0) / output.shape[0]) * 100
        print("\nTop{0} Accuracy:\n{1}%".format(topN_error, accuracy))
        
        
        """Error - Sum of wrong classifications"""
        # Setting 0.5 threshold for output binary units
        _temp = np.empty(output.shape)
        _temp.fill(0.5)
        output_binary = output >= _temp
        
        err  = output_binary.astype(int) - target
        
        # Error is calculated by dividing the total number of differences in the target and the output np.sum(err**2), by the total number of elements
        # Total number of elements is considered due to the fact that it is Multi Label classification problem, where we should count the number 
        # of correctly labeled classes in every row
        error = np.sum(err**2) / (err.shape[0] * err.shape[1])
        print("\nError:\n{0}%".format(error * 100.0))
        
        
        """F1 Score & Accuracy"""
        tp = 0.0
        fp = 0.0
        tn = 0.0
        fn = 0.0
        # Flattening the matrix [[1,2,3], [4,5,6]] => [1,2,3,4,5,6]
        output_flat = output_binary.astype(int).flatten()
        target_flat = target.flatten()
        
        for i in range(len(output_flat)):
            # if positive
            if target_flat[i] == 1:
                if output_flat[i] == 1:
                    tp += 1
                else:
                    fp += 1
            
            # if negative
            if target_flat[i] == 0:
                if output_flat[i] == 0:
                    tn += 1
                else:
                    fn += 1
        
        _precission = tp / (tp + fp)
        _recall = tp / (tp + fn)
        
        accuracy = (tp + tn) / (tp + tn + fp + fn)
        f1Score = 2 * ((_precission * _recall) / (_precission + _recall))
        
        print("\nAccuracy:\n{0}%\n\nF1-Score:\n{1}".format(accuracy * 100, f1Score))

        
        
        
# If run as a script, create a test object
if __name__ == "__main__":
    
    dbn = DBN((48, 30, 20, 10, 5))
    
    train_input, train_target, test_input, test_target = GetSCAREdata_Sampled()
    
    dbn.PreTrain(train_input)
    
    """Save the Weight Matrices"""
    f = file("Weights_init.save", "wb")
    for obj in [dbn.w, dbn.b]:
        cp.dump(obj, f, cp.HIGHEST_PROTOCOL)
    f.close()    
    
    """Load the saved Weights"""
    #f = file("Weights_init.save", "rb")
    #objs = []
    #for i in range(2):
    #    objs.append(cp.load(f))
    #dbn.w = objs[0]
    #dbn.b = objs[1]
    #f.close()    
    
    print("********Weights are Initialized, Fine Tuning now********\n")
    
    dnn = dbn.Train(train_input, train_target)
    
    """Save the Weight Matrices"""
    f = file("Weights.save", "wb")
    for obj in [dnn.w, dnn.b]:
        cp.dump(obj, f, cp.HIGHEST_PROTOCOL)
    f.close()    
    
    dbn.Evaluate(dnn, test_input, test_target)
    
    
    """Display the Output"""               
    #output = dnn.Run(input)
    #print("Input:\n{0}\n\n Output:\n{1}".format(input, output))

    