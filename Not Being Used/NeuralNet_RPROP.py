# Imports
import numpy as np
from numpy import empty
from Datasets import *


class NeuralNet:
    
    """Initializing weights and biases"""
    # weights and momentum the term
    w = []
    w_m = []
    # biases and the momentum term
    b = []
    b_m = []
    
    """Initializing other terms"""
    # shape of the network (4,3,5) -> 4 visible units, 3 hidden units, 5 hidden units in last layer
    shape = None
    # Number of layers excluding visible layer, 2 for the case above
    layerCount = 0
    # initial and final momentum
    momentum_i = 0.5
    momentum_f = 0.9
    # Weight Decay L2 Penalty
    w_cost = 0.0002
    # learning rate
    e = 1
    
    """Initializing terms for the RPROP which however is not being used"""
    update_val_max = 50
    update_val_min = 1e-6
    w_update_val = []
    b_update_val = []
    inc = 1.2
    dec = 0.5
    w_prev_grad = []
    b_prev_grad = []
    
    
    """Initialize the network"""
    def __init__(self, shape, weights = [], biases = []):
        
        self.layerCount = len(shape) - 1
        self.shape = shape

        self._layerInput = []
        self._layerOutput = []

        # Random initialization of weights and biases if Pre-Training is not performed  
        if not weights and not biases:
            for (l1, l2) in zip(shape[:-1], shape[1:]):         
                self.w.append(np.random.normal(scale=0.1, size=(l1, l2)))
                self.b.append(np.random.normal(scale=0.1, size=(1, l2)))
        else:
            self.w = weights
            self.b = biases
            
        # Initializing terms for momentum
        for (l1, l2) in zip(shape[:-1], shape[1:]):
            self.w_m.append(np.zeros(shape=(l1,l2)))
            self.b_m.append(np.zeros(shape=(1,l2)))
            
        # Initializing terms for RPROP
        for (l1, l2) in zip(shape[:-1], shape[1:]):
            # Previous gradients (weight update values if not using RPROP)
            self.w_prev_grad.append(np.zeros(shape=(l1,l2)))
            self.b_prev_grad.append(np.zeros(shape=(1,l2)))
            # Weight update values initialized to 0.1
            self.w_update_val.append(np.ones(shape=(l1,l2))*0.1)
            self.b_update_val.append(np.ones(shape=(1,l2))*0.1)
    
    
    """Run the network on the Input data"""
    def Run(self, data):
        
        # Number of training samples
        n = data.shape[0]
        
        # Clear out the values from the last run
        self._layerInput = []
        self._layerOutput = []
        
        # Feed forward the training data and get the output
        for layer in range(self.layerCount):
            
            # Input coming from visible units is the data itself
            if layer == 0:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, data, self.w[layer]))
                layerInput = np.dot(data, self.w[layer]) + np.tile(self.b[layer], (n,1))
            else:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, self._layerOutput[-1], self.w[layer]))
                layerInput = np.dot(self._layerOutput[-1], self.w[layer]) + np.tile(self.b[layer], (n,1))
            
            self._layerInput.append(layerInput)
            
            # if last layer, use softmax
            """if layer == self.layerCount - 1:
                self._layerOutput.append(self.Softmax(layerInput))
            else:
                self._layerOutput.append(self.Sigmoid(layerInput))"""
            self._layerOutput.append(self.Sigmoid(layerInput))
        return self._layerOutput[-1]
        
        
    """Train the network - Back Propagation"""
    def TrainEpoch(self, input, target):
        
        err_delta = []
        n = input.shape[0]
        
        # Input the data and get the output from an initialized network
        self.Run(input)
        
        # Calculate the error deltas
        for layer in reversed(range(self.layerCount)):
            
            # For the last layer, compare to the target values
            if layer == self.layerCount - 1:
                err = self._layerOutput[layer] - target
                err_delta.append(err * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
                
                # For the evaluation
                error_eval = np.sum(err**2)
            
            # Back propagate the error for the rest of the layers
            else:
                err_backProp = np.dot(err_delta[-1], self.w[layer + 1].T)
                err_delta.append(err_backProp * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
            
        # Compute updated weights
        for layer in range(self.layerCount):
            
            #       For layer = 0 1 2 3
            # err_delta_index = 3 2 1 0
            err_delta_index = self.layerCount - 1 - layer
            
            # For the first layer, activation^i = input vector
            if layer == 0:
                activation_i = input
            else:
                activation_i = self._layerOutput[layer - 1]
            
            ones = np.ones(shape=(n,1))
            
            # [layers{0}, rows{1}, columns{2]]
            # [None,:,:] Creates a third dimension at 0 index, so a[0,:,:] or a[0] will display all the rows and columns of first layer
            # transpose(1,2,0) re-organizing these makes every layer represent a sample, with states of units in separate rows 
            # transpose(1,0,2) re-organizing these makes every layer represent a sample, with states of units in separate columns 
            w_update = np.sum(activation_i[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            b_update = np.sum(ones[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            
            momentum = self.momentum_i
            if error_eval <= 100:
                momentum = self.momentum_f
            
            """# Weight update with momentum
            self.w_m[layer] = (momentum * self.w_m[layer]) + (self.e * w_update) - (self.w_cost * self.w[layer])
            self.w[layer] -= self.w_m[layer]
            self.b_m[layer] = (momentum * self.b_m[layer]) + (self.e * b_update) - (self.w_cost * self.b[layer])
            self.b[layer] -= self.b_m[layer]"""
            
            """# Weight update without momentum
            self.w[layer] -= self.e * w_update
            self.b[layer] -= self.e * b_update"""
            
            # RPROP
            # Determining if gradient sign is changed
            w_product = np.multiply(self.w_prev_grad[layer], w_update)
            b_product = np.multiply(self.b_prev_grad[layer], b_update)
            
            index = 0
            for prod in w_product.flat:
                # If gradient sign did not change, increment the update value
                if prod > 0:
                    self.w_update_val[layer].flat[index] = min(self.w_update_val[layer].flat[index] * self.inc, self.update_val_max)
                # If gradient sign did change, decrement the update value
                if prod < 0:
                    self.w_update_val[layer].flat[index] = max(self.w_update_val[layer].flat[index] * self.dec, self.update_val_min)
                index += 1
            self.w_prev_grad[layer] = w_update
            
            index = 0
            for prod in b_product.flat:
                if prod > 0:
                    self.b_update_val[layer].flat[index] = max(self.b_update_val[layer].flat[index] * self.inc, self.update_val_max)
                if prod < 0:
                    self.b_update_val[layer].flat[index] = min(self.b_update_val[layer].flat[index] * self.dec, self.update_val_min)
                index += 1
            self.b_prev_grad[layer] = b_update
            
            # Add the update value if gradient is positive, and subtract if otherwise
            self.w[layer] -= np.multiply(np.sign(w_update), self.w_update_val[layer])
            self.b[layer] -= np.multiply(np.sign(b_update), self.b_update_val[layer])
            
        return error_eval
        
        
    """Activation Functions"""
    def Sigmoid(self, x):
        return 1 / (1 + np.exp(-x))
    
    def Softmax(self, x):
        return np.exp(x) / np.tile(np.sum(np.exp(x), axis=1)[np.newaxis].T, (1, x.shape[1]))
            
            
    
if __name__ == "__main__":
    nn = NeuralNet((6, 4, 1))
    
    input, target = GetData()
    #input = input[0:2000, :]
    #target = target[0:2000, :]

    lnMax = 100000
    #lnMax = 1000
    lnErr = 1e-5
    for i in range(lnMax+1):
        err = nn.TrainEpoch(input, target)
        if i % 2500 == 0:
            print("Iteration {0}\tError: {1:0.6f}".format(i, err))
        if err <= lnErr:
            print("\nMinimum error reached at iteration {0}\n".format(i))
            break
    
    #Display Output               
    output = nn.Run(input)
    print("Input:\n{0}\n\n Output:\n{1}".format(input, output))
    print(nn.w)
    print(nn.b)
