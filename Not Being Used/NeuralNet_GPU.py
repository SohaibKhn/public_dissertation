# Imports
import gnumpy as gp
from numpy import empty
from Datasets import *


class NeuralNet:
    
    """Initializing weights and biases"""
    # weights and momentum the term
    w = []
    w_m = []
    # biases and the momentum term
    b = []
    b_m = []
    
    """Initializing other terms"""
    # shape of the network (4,3,5) -> 4 visible units, 3 hidden units, 5 hidden units in last layer
    shape = None
    # Number of layers excluding visible layer, 2 for the case above
    layerCount = 0
    # initial and final momentum
    momentum_i = 0.5
    momentum_f = 0.9
    # Weight Decay L2 Penalty
    w_cost = 0.0002
    # learning rate
    e = 0.001

    
    """Initialize the network"""
    def __init__(self, shape, weights = [], biases = []):
        
        self.layerCount = len(shape) - 1
        self.shape = shape

        self._layerInput = []
        self._layerOutput = []

        # Random initialization of weights and biases if Pre-Training is not performed  
        if not weights and not biases:
            for (l1, l2) in zip(shape[:-1], shape[1:]):         
                self.w.append(0.1 * gp.randn((l1, l2)))
                self.b.append(0.1 * gp.randn((1, l2)))
        else:
            self.w = weights
            self.b = biases
            
        # Initializing terms for momentum
        for (l1, l2) in zip(shape[:-1], shape[1:]):
            self.w_m.append(gp.zeros(shape=(l1,l2)))
            self.b_m.append(gp.zeros(shape=(1,l2)))
    
    
    """Run the network on the Input data"""
    def Run(self, data):
        
        # Number of training samples
        n = data.shape[0]
        
        # Clear out the values from the last run
        self._layerInput = []
        self._layerOutput = []
        
        # Feed forward the training data and get the output
        for layer in range(self.layerCount):
            
            # Input coming from visible units is the data itself
            if layer == 0:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, data, self.w[layer]))
                layerInput = gp.dot(data, self.w[layer]) + self.b[layer]
            else:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, self._layerOutput[-1], self.w[layer]))
                layerInput = gp.dot(self._layerOutput[-1], self.w[layer]) + self.b[layer]
            
            self._layerInput.append(layerInput)
            
            # if last layer, use softmax
            if layer == self.layerCount - 1:
                self._layerOutput.append(self.Softmax(layerInput))
            else:
                self._layerOutput.append(gp.logistic(layerInput))
            
        return self._layerOutput[-1]
        
        
    """Train the network - Back Propagation"""
    def TrainEpoch(self, input, target):
        
        err_delta = []
        n = input.shape[0]
        
        # Input the data and get the output from an initialized network
        self.Run(input)
        
        # Calculate the error deltas
        for layer in reversed(range(self.layerCount)):
            
            # For the last layer, compare to the target values
            if layer == self.layerCount - 1:
                err = self._layerOutput[layer] - target
                err_delta.append(err * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
                
                # For the evaluation
                error_eval = gp.sum(err**2) / n
            
            # Back propagate the error for the rest of the layers
            else:
                err_backProp = gp.dot(err_delta[-1], self.w[layer + 1].T)
                err_delta.append(err_backProp * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
            
        # Compute updated weights
        for layer in range(self.layerCount):
            
            #       For layer = 0 1 2 3
            # err_delta_index = 3 2 1 0
            err_delta_index = self.layerCount - 1 - layer
            
            # For the first layer, activation^i = input vector
            if layer == 0:
                activation_i = input
            else:
                activation_i = self._layerOutput[layer - 1]
            
            ones = gp.ones(shape=(n,1))
            
            # [layers{0}, rows{1}, columns{2]]
            # [None,:,:] Creates a third dimension at 0 index, so a[0,:,:] or a[0] will display all the rows and columns of first layer
            # transpose(1,2,0) re-organizing these makes every layer represent a sample, with states of units in separate rows 
            # transpose(1,0,2) re-organizing these makes every layer represent a sample, with states of units in separate columns 
            w_update = gp.sum(activation_i[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            b_update = gp.sum(ones[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            
            momentum = self.momentum_i
            if error_eval <= 0.5:
                momentum = self.momentum_f
            
            # Weight update with momentum
            self.w_m[layer] = (momentum * self.w_m[layer]) + (self.e * w_update) - (self.w_cost * self.w[layer])
            self.w[layer] -= self.w_m[layer]
            self.b_m[layer] = (momentum * self.b_m[layer]) + (self.e * b_update) - (self.w_cost * self.b[layer])
            self.b[layer] -= self.b_m[layer]
            
            """# Weight update without momentum
            self.w[layer] -= self.e * w_update
            self.b[layer] -= self.e * b_update"""
            
        return error_eval
        
        
    """Activation Functions"""
    def Sigmoid(self, x):
        return 1 / (1 + gp.exp(-x))
    
    def Softmax(self, x):
        return gp.exp(x) / gp.sum(gp.exp(x), axis=1)[gp.newaxis].T
            
            
    
if __name__ == "__main__":
    nn = NeuralNet((89, 60, 40, 15, 8))
    
    input, target = GetKYOTOdata()
    input = gp.garray(input[0:2000, :])
    target = gp.garray(target[0:2000, :])

    epochs = 100
    
    lnErr = 1e-5
    for epoch in range(epochs):
        err = nn.TrainEpoch(input, target)
        
        if epoch % 10 == 0:
            print("Iteration {0}\tError: {1:0.6f}".format(epoch, err))
        if err <= lnErr:
            print("\nMinimum error reached at iteration {0}\n".format(epoch))
            break
    
    """Display Output"""               
    #output = nn.Run(input)
    #print("Input:\n{0}\n\n Output:\n{1}".format(input, output))
    #print(nn.w)
    #print(nn.b)
    
    """Evaluation"""
    # Run the trained network on Test Data and get the output
    output = nn.Run(input[0:5, :])
    print("Target:\n{0}\n\n Output:\n{1}".format(target[0:5, :], output))
    
    # Sort the output in ascending order and return the index of elements
    # [20,30,10,50] => [3, 1, 0, 2]
    # Here the indexes represent the output units. In this case highest value 50 is given by the 4th output unit, and least 10 by the 3rd
    outputIndex = np.argsort(output.as_numpy_array(), axis=1)
    
    # Compare the target with the top N output probabilities
    topN_error = 1
    error = outputIndex[:, -topN_error:] == np.argmax(target[0:5, :].as_numpy_array(), axis=1)[np.newaxis].T
    accuracy = (np.sum(error) / output.shape[0]) * 100
    print("\nAccuracy:\n{0}%".format(accuracy))
    