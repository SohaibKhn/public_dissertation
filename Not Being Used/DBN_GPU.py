# Imports
import gnumpy as gp
from RBM_GPU import *
from NeuralNet_GPU import *
from Datasets import *


class DBN:
    
    """Initializing weights and biases"""
    # weights
    w = []
    # biases
    b = []
    # shape of the network (4,3,5) -> 4 visible units, 3 hidden units, 5 hidden units in last layer
    shape = None
    # Number of layers excluding visible layer
    layerCount = 0
    
    
    """Initialize the network"""
    def __init__(self, shape):
        
        self.layerCount = len(shape) - 1
        self.shape = shape
        
    
    
    """Learn initial values of weights and biases by training RBMs"""
    def PreTrain(self, data):
        
        # For every layer train an RBM
        for layer in range(self.layerCount):
            
            print("Input data for layer {0}: \n {1}\n".format(layer+1, data))
            
            rbm = RBM(number_of_visible_units = self.shape[layer], number_of_hidden_units = self.shape[layer+1])
            rbm.Train(data, epochs = 10)
            
            self.w.append(rbm.w)
            self.b.append(rbm.b_h)
            
            # Generate the hidden units' states from learned RBM and use them as an input to the next RBM
            data = rbm.Generate(data, "h")
            
            # Remove object to free up the memory
            del(rbm)
            
        print("Weights:\n{0}\n".format(self.w))
        print("Biases:\n{0}\n".format(self.b))
    
    
    
    """Train a deep neural network using pre-trained weights"""
    def Train(self, input, target):
        nn = NeuralNet(self.shape, dbn.w, dbn.b)
    
        epochs = 10
        lnErr = 1e-5
        for epoch in range(epochs):
            err = nn.TrainEpoch(input, target)
            
            if epoch % 2 == 0:
                print("Iteration {0}\tError: {1:0.6f}".format(epoch, err))
            if err <= lnErr:
                print("\nMinimum error reached at iteration {0}\n".format(epoch))
                break
        return nn
    


    """"Returns the accuracy"""
    def Evaluate(self, DNN, input, target, topN_errorRate):
            
        # Run the trained network on Test Data and get the output
        output = DNN.Run(input)
        print("Target:\n{0}\n\n Output:\n{1}".format(target, output))
        
        # Sort the output in ascending order and return the index of elements
        # [20,30,10,50] => [3, 1, 0, 2]
        # Here the indexes represent the output units. In this case highest value 50 is given by the 4th output unit, and least 10 by the 3rd
        outputIndex = np.argsort(output.as_numpy_array(), axis=1)
        
        # Compare the target with the top N output probabilities
        topN_error = 1
        error = outputIndex[:, -topN_error:] == np.argmax(target.as_numpy_array(), axis=1)[np.newaxis].T
        accuracy = (np.sum(error) / output.shape[0]) * 100
        return accuracy


        
# If run as a script, create a test object
if __name__ == "__main__":
    dbn = DBN([89, 60, 40, 15, 8])
    input, target = GetKYOTOdata()
    input_train = input[0:11000, :]
    target_train = target[0:11000]
    dbn.PreTrain(gp.garray(input_train))
    
    print("********Weights are Initialized, Fine Tuning now********\n")
    
    dnn = dbn.Train(gp.garray(input_train), gp.garray(target_train))
    
    accuracy = dbn.Evaluate(dnn, gp.garray(input[11000:12417, :]), gp.garray(target[11000:12417, :]), topN_errorRate = 1)
    print("\nAccuracy:\n{0}%".format(accuracy))

    #Display Output               
    #output = dnn.Run(gp.garray(input))
    #print("Input:\n{0}\n\n Output:\n{1}".format(input, output))

    