# Imports
import numpy as np
import gnumpy as gp
from numpy import dtype
from Datasets import *


class RBM:
    
    """Initializing weights and biases"""
    # weights and momentum term
    w = []
    w_m = []
    # biases for hidden units and momentum term
    b_h = []
    b_h_m = []
    # biases for visible units and momentum term
    b_v = []
    b_v_m = []
    
    """Initializing other terms"""
    # number of visible units
    i = 0
    # number of hidden units
    j = 0
    # learning rate
    e = 0.001
    # momentum
    momentum = 0.5
    # weight penalty
    w_cost = 0.0002
    # size of the mini batch
    n = 10
    
    
    """Initialize the network"""
    def __init__(self, number_of_visible_units, number_of_hidden_units):
        
        self.i = number_of_visible_units
        self.j = number_of_hidden_units

        # Initializing weights to small random values chosen from a zero-mean Gaussian with a 0.01 standard deviation
        self.w = 0.01 * np.random.randn(self.i, self.j)
          
        # Setting the hidden and visible biases to 0
        self.b_h = np.zeros((1, self.j))
        self.b_v = np.zeros((1, self.i))
        
        # Setting momentum term to 0
        self.w_m = np.zeros((self.i, self.j))
        self.b_h_m = np.zeros((1, self.j))
        self.b_v_m = np.zeros((1, self.i))
    
    
    """Activation Functions"""    
    def Sigmoid(self, x):
        return 1.0 / (1 + np.exp(-x))
      
        
    """Learn the Weights"""
    def Train(self, data, epochs):
        
        # Number of batches
        N = data.shape[0]
        batches = N / self.n
        
        for epoch in range(epochs):  
            
            # To sum up the error of all batches
            err = []
            
            for batch in range(batches):
                
                # Clamp a batch of input vectors on the visible units
                v_states = data[self.n*batch : self.n*(batch + 1)]
                
                """Contrastive Divergence"""
                for cd in range(1):
                    
                    # Positive Phase
                    h_activations = gp.dot(gp.garray(v_states), gp.garray(self.w)).as_numpy_array() + self.b_h 
                    h_probs = self.Sigmoid(h_activations)
                
                    # "and the hidden unit turns on if this probability is greater than a random number uniformly distributed between 0 and 1" (Hinton)
                    h_states = h_probs > np.random.rand(self.n, self.j)
                
                    # Measure Positive Statistics <ViHj>0
                    if cd == 0:
                        positive_stats_w = gp.dot(gp.garray(v_states).T, gp.garray(h_states)).as_numpy_array()
                        positive_stats_b_h = sum(h_states)
                        positive_stats_b_v = sum(v_states)
                    
                    # Negative Phase - Reconstruction
                    v_activations = gp.dot(gp.garray(h_states), gp.garray(self.w).T).as_numpy_array() + self.b_v
                    v_probs = self.Sigmoid(v_activations)
                
                    # It is common to use probabilities for visible units
                    v_states = v_probs
            
                h_activations = gp.dot(gp.garray(v_probs), gp.garray(self.w)).as_numpy_array()
                h_probs = self.Sigmoid(h_activations)
            
                # Measure Negative Statistics <ViHj>inf
                negative_stats_w = gp.dot(gp.garray(v_probs).T, gp.garray(h_probs)).as_numpy_array()
                negative_stats_b_h = sum(h_probs)
                negative_stats_b_v = sum(v_probs)
            
                # Update weights.
                self.w_m = (self.w_m * self.momentum) + (positive_stats_w - negative_stats_w) - (self.w_cost * self.w)
                self.w += (self.e * self.w_m) / self.n
            
                self.b_h_m = (self.b_h_m * self.momentum) + (positive_stats_b_h - negative_stats_b_h) - (self.w_cost * self.b_h)
                self.b_h += (self.e * self.b_h_m) / self.n
            
                self.b_v_m = (self.b_v_m * self.momentum) + (positive_stats_b_v - negative_stats_b_v) - (self.w_cost * self.b_v)
                self.b_v += (self.e * self.b_v_m) / self.n
            
                """# Update weights without momentum
                self.w += self.e * ((positive_stats_w - negative_stats_w) / self.n)
                self.b_h += self.e * ((positive_stats_b_h - negative_stats_b_h) / self.n)
                self.b_v += self.e * ((positive_stats_b_v - negative_stats_b_v) / self.n)"""
                
                err.append((data[self.n*batch : self.n*(batch + 1)] - v_probs)**2)  
                   
            error = np.mean(err)             
            
            if epoch % 2 == 0:
                print("Epoch %s: error is %s\n" % (epoch, error))
    
    
    
    """To Run the Generative Model - let the trained RBM produce hidden samples if layer=h and re-produce the input samples if layer=v"""
    def Generate(self, data, layer="h"):
        
        n = data.shape[0]
        
        h_activations = gp.dot(gp.garray(data), gp.garray(self.w)).as_numpy_array()
        h_probs = self.Sigmoid(h_activations)
        h_states = h_probs > np.random.rand(n, self.j)
        
        if layer=="h":
            return h_states
        
        v_activations = gp.dot(gp.garray(h_states), gp.garray(self.w).T).as_numpy_array()
        v_probs = self.Sigmoid(v_activations)
        v_states = v_probs > np.random.rand(n, self.i)
        
        return v_states
        
             

if __name__ == '__main__':
    rbm = RBM(number_of_visible_units = 89, number_of_hidden_units = 50)
    input, target = GetKYOTOdata()
    rbm.Train(input[0:1000, :], epochs = 200)
    #print(rbm.w)
    print(rbm.b_h)
    print(rbm.b_v)
    #vec = np.array([[1,1,1,0,0,0], [0,1,1,0,0,0], [1,0,1,0,0,0], [0,0,1,0,0,0], [0,0,0,1,1,0], [0,0,0,1,1,1], [0,1,0,1,1,0]])
    #print(rbm.Generate(vec, "v"))

