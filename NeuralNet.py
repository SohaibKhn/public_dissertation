# Imports
import numpy as np
from numpy import empty
from Datasets import *
import pickle as cp


class NeuralNet:
    
    """Initializing weights and biases"""
    # weights and momentum the term
    w = []
    w_m = []
    # biases and the momentum term
    b = []
    b_m = []
    
    """Initializing other terms"""
    # shape of the network (4,3,5) -> 4 visible units, 3 hidden units, 5 hidden units in last layer
    shape = None
    # Number of layers excluding visible layer, 2 for the case above
    layerCount = 0
    # initial and final momentum
    momentum_i = 0.5
    momentum_f = 0.9
    # Weight Decay L2 Penalty
    w_cost = 0.0002
    # learning rate
    e = 0.001

    
    """Initialize the network"""
    def __init__(self, shape, weights = [], biases = []):
        
        self.layerCount = len(shape) - 1
        self.shape = shape

        self._layerInput = []
        self._layerOutput = []

        # Random initialization of weights and biases if Pre-Training is not performed  
        if not weights and not biases:
            for (l1, l2) in zip(shape[:-1], shape[1:]):         
                self.w.append(np.random.normal(scale=0.1, size=(l1, l2)))
                self.b.append(np.random.normal(scale=0.1, size=(1, l2)))
        else:
            self.w = weights
            self.b = biases
            
        # Initializing terms for momentum
        for (l1, l2) in zip(shape[:-1], shape[1:]):
            self.w_m.append(np.zeros(shape=(l1,l2)))
            self.b_m.append(np.zeros(shape=(1,l2)))
    
    
    """Run the network on the Input data"""
    def Run(self, data):
        
        # Number of training samples
        n = data.shape[0]
        
        # Clear out the values from the last run
        self._layerInput = []
        self._layerOutput = []
        
        # Feed forward the training data and get the output
        for layer in range(self.layerCount):
            
            # Input coming from visible units is the data itself
            if layer == 0:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, data, self.w[layer]))
                layerInput = np.dot(data, self.w[layer]) + self.b[layer]
            else:
                #print("\n\n\t\tLayer: {0}\nData:\n{1}\n\nWeights:\n{2}".format(layer, self._layerOutput[-1], self.w[layer]))
                layerInput = np.dot(self._layerOutput[-1], self.w[layer]) + self.b[layer]
            
            self._layerInput.append(layerInput)
            
            # if last layer, use softmax
            """if layer == self.layerCount - 1:
                self._layerOutput.append(self.Softmax(layerInput))
            else:
                self._layerOutput.append(self.Sigmoid(layerInput))"""
                
            self._layerOutput.append(self.Sigmoid(layerInput))
        
        return self._layerOutput[-1]
        
        
    """Train the network - Back Propagation"""
    def TrainEpoch(self, input, target):
        
        err_delta = []
        n = input.shape[0]
        
        # Input the data and get the output from an initialized network
        self.Run(input)
        
        # Calculate the error deltas
        for layer in reversed(range(self.layerCount)):
            
            # For the last layer, compare to the target values
            if layer == self.layerCount - 1:
                err = self._layerOutput[layer] - target
                err_delta.append(err * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
                
                # For the evaluation
                error_eval = np.sum(err**2) / n
            
            # Back propagate the error for the rest of the layers
            else:
                err_backProp = np.dot(err_delta[-1], self.w[layer + 1].T)
                err_delta.append(err_backProp * self._layerOutput[layer] * (1 - self._layerOutput[layer]))
            
        # Compute updated weights
        for layer in range(self.layerCount):
            
            #       For layer = 0 1 2 3
            # err_delta_index = 3 2 1 0
            err_delta_index = self.layerCount - 1 - layer
            
            # For the first layer, activation^i = input vector
            if layer == 0:
                activation_i = input
            else:
                activation_i = self._layerOutput[layer - 1]
            
            ones = np.ones(shape=(n,1))
            
            # [layers{0}, rows{1}, columns{2]]
            # [None,:,:] Creates a third dimension at 0 index, so a[0,:,:] or a[0] will display all the rows and columns of first layer
            # transpose(1,2,0) re-organizing these makes every layer represent a sample, with states of units in separate rows 
            # transpose(1,0,2) re-organizing these makes every layer represent a sample, with states of units in separate columns 
            w_update = np.sum(activation_i[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            b_update = np.sum(ones[None,:,:].transpose(1,2,0) * err_delta[err_delta_index][None,:,:].transpose(1,0,2), axis=0)
            
            momentum = self.momentum_i
        
            if error_eval <= 0.3:
                momentum = self.momentum_f
                #if error_eval <= 0.2:
                #    self.e = 0.0001
            
            # Weight update with momentum
            self.w_m[layer] = (momentum * self.w_m[layer]) + (self.e * w_update) - (self.w_cost * self.w[layer])
            self.w[layer] -= self.w_m[layer]
            self.b_m[layer] = (momentum * self.b_m[layer]) + (self.e * b_update) - (self.w_cost * self.b[layer])
            self.b[layer] -= self.b_m[layer]
            
            # Weight update without momentum
            """self.w[layer] -= self.e * w_update
            self.b[layer] -= self.e * b_update"""
            
        return error_eval
        
        
    """Activation Functions"""
    def Sigmoid(self, x):
        return 1 / (1 + np.exp(-x))
    
    def Softmax(self, x):
        return np.exp(x) / np.sum(np.exp(x), axis=1)[np.newaxis].T
            
            
    
if __name__ == "__main__":
    
    """Load the saved Weights"""
    f = file("1.save", "rb")
    objs = []
    for i in range(2):
        objs.append(cp.load(f))
    _W = objs[0]
    _B = objs[1]
    f.close()
    
    train_input, train_target, test_input, test_target = GetSCAREdata_Sampled()
    
    nn = NeuralNet((train_input.shape[1], 35, 25, 15, 10, train_target.shape[1]), _W, _B)
    
    epochs = 1000
    
    #lnErr = 1e-5
    #for epoch in range(epochs+1):
    #    err = nn.TrainEpoch(train_input, train_target)
    #   
    #    if epoch % 10 == 0:
    #        print("Iteration {0}\tError: {1:0.6f}".format(epoch, err))
    #    if err <= lnErr:
    #        print("\nMinimum error reached at iteration {0}\n".format(epoch))
    #        break
    
    
    """Save the Weight Matrices"""
    #f = file("Weights.save", "wb")
    #for obj in [nn.w, nn.b]:
    #    cp.dump(obj, f, cp.HIGHEST_PROTOCOL)
    #f.close()
    
    
    """Display Output"""               
    #output = nn.Run(input)
    #print("Input:\n{0}\n\n Output:\n{1}".format(input, output))
    #print(nn.w)
    #print(nn.b)
    
    
    """Evaluation"""
    # Run the trained network on Test Data and get the output
    output = nn.Run(test_input)
    print("Target:\n{0}\n\n Output:\n{1}".format(test_target, output))
    
    
    """TopN Accuracy"""
    # Sort the output in ascending order and return the index of elements
    # [20,30,10,50] => [3, 1, 0, 2]
    # Here the indexes represent the output units. In this case highest value 50 is given by the 4th output unit, and least 10 by the 3rd
    outputIndex = np.argsort(output, axis=1)
    
    # Compare the target with the top N output probabilities
    topN_error = 3
    correct = outputIndex[:, -topN_error:] == np.argmax(test_target, axis=1)[np.newaxis].T
    accuracy = ((np.sum(correct)*1.0) / output.shape[0]) * 100
    print("\nTop{0} Accuracy:\n{1}%".format(topN_error, accuracy))
    
    
    """Error - Sum of wrong classifications"""
    # Setting 0.5 threshold for output binary units
    _temp = np.empty(output.shape)
    _temp.fill(0.5)
    output_binary = output >= _temp
    
    err  = output_binary.astype(int) - test_target
    
    # Error is calculated by dividing the total number of differences in the target and the output np.sum(err**2), by the total number of elements
    # Total number of elements is considered due to the fact that it is Multi Label classification problem, where we should count the number 
    # of correctly labeled classes in every row
    error = np.sum(err**2) / (err.shape[0] * err.shape[1])
    print("\nError:\n{0}%".format(error * 100.0))
    
    
    """F1 Score & Accuracy"""
    tp = 0.0
    fp = 0.0
    tn = 0.0
    fn = 0.0
    # Flattening the matrix [[1,2,3], [4,5,6]] => [1,2,3,4,5,6]
    output_flat = output_binary.astype(int).flatten()
    target_flat = test_target.flatten()
    
    for i in range(len(output_flat)):
        # if positive
        if target_flat[i] == 1:
            if output_flat[i] == 1:
                tp += 1
            else:
                fp += 1
        
        # if negative
        if target_flat[i] == 0:
            if output_flat[i] == 0:
                tn += 1
            else:
                fn += 1
    
    _precission = tp / (tp + fp)
    _recall = tp / (tp + fn)
    
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    f1Score = 2 * ((_precission * _recall) / (_precission + _recall))
    
    print("\nAccuracy:\n{0}%\n\nF1-Score:\n{1}".format(accuracy * 100, f1Score))
            
    